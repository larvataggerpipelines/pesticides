SHELL:=/bin/bash

TAGGER:=20230311
LARVATAGGER_IMAGE=flaur/larvatagger:0.16.4-$(TAGGER)
MWT_IMAGE:=quay.io/flaur/mwt-container:0.2.1

# Lautaro's data
DATA_PATH:=/g/crocker/Lautaro/Drugs_screen/Drosophila_data
DATA_PATH:=/g/crocker/Lautaro/Genetic_basis_resistance/GstD1
EXTERNAL_RAW=$(DATA_PATH)/Videos
EXTERNAL_INTERIM=$(DATA_PATH)/Automated analysis - Temporary files
EXTERNAL_PROCESSED=$(DATA_PATH)/Automated_analysis
TARGET_LARVAE_NR:=target_larva_counts.csv

# Johannes' data
#DATA_PATH:=/g/crocker/Johannes/Behavior_results
#EXTERNAL_PROCESSED=$(DATA_PATH)/Automated_analysis
#TARGET_LARVAE_NR:=none

TEST_RAW:=$(HOME)/Videos
TEST_INTERIM:=$(HOME)/Automated analysis - Temporary files
TEST_PROCESSED:=$(HOME)/Automated analysis
TEST_FLAGS=EXTERNAL_RAW='$(TEST_RAW)' EXTERNAL_INTERIM='$(TEST_INTERIM)' EXTERNAL_PROCESSED='$(TEST_PROCESSED)' PIPELINE_TEST=1
#TEST_FLAGS=EXTERNAL_INTERIM='$(TEST_INTERIM)' EXTERNAL_PROCESSED='$(TEST_PROCESSED)' PIPELINE_TEST=1

CURRENT_DIR:=$(realpath $(CURDIR))

.PHONY: default bin data clean compute permissions update fromscratch fromscratch-1 compress test

default: bin data permissions

bin: bin/larvatagger.sif bin/mwt-container.sif
	cd src/TrackingQualityControl && $(MAKE)

bin/mwt-container.sif:
	apptainer build $@ docker://$(MWT_IMAGE)

bin/larvatagger.sif:
	apptainer build $@ docker://$(LARVATAGGER_IMAGE)

clean:
	# explicitly clear the content of data/interim, because it may also be a symbolic link
	rm -rf "$(realpath data/interim)" || true
	# symbolic links are removed (e.g. raw and processed), but not the actual target directories
	rm -rf data/{raw,interim,processed}
	rm -rf log

data: data/interim data/raw data/processed
	# data/interim comes first, because this target is responsible for creating the data directory

data/raw:
	ln -sf "$(EXTERNAL_RAW)" data/raw

data/processed:
	mkdir -p "$(EXTERNAL_PROCESSED)"
	chmod g+w "$(EXTERNAL_PROCESSED)"
	ln -sf "$(EXTERNAL_PROCESSED)" data/processed

ifeq "$(firstword $(subst /, ,$(CURRENT_DIR)))" "home"
data/interim:
	mkdir -p data
	mkdir -p "$(EXTERNAL_INTERIM)"
	chmod g+w "$(EXTERNAL_INTERIM)"
	ln -sf "$(EXTERNAL_INTERIM)" data/interim
else
data/interim:
	mkdir -p data/interim
endif

permissions:
	chmod -Rf g+w {src,bin} || true
	[ -d data/interim ] && chmod -Rf g+w "$(realpath data/interim)" || true
	[ -d log ] && chmod -Rf g+w log || true

compute: default
	rm -rf log
	FROM_MAKEFILE=1 TAGGER=$(TAGGER) TARGET_LARVAE_NR=$(TARGET_LARVAE_NR) bin/compute.sh

update: clean
	git stash
	git pull

fromscratch: update
	# reload the Makefile
	$(MAKE) fromscratch-1

fromscratch-1: data
	# clear the content of the target directory, but not the directory per se
	rm -rf data/processed/*
	bin/compute.sh

compress: data
	cd data/processed && srun tar zcvf ../interim/processed.tar.gz *
	@echo "The compressed data are available at: "
	@echo "$(realpath data/interim/processed.tar.gz)"

test:
	rm -f data/{raw,processed}
	rm -rf data/interim
	rm -rf "$(TEST_INTERIM)"; mkdir -p "$(TEST_INTERIM)"
	rm -rf "$(TEST_PROCESSED)"; mkdir -p "$(TEST_PROCESSED)"
	$(MAKE) $(TEST_FLAGS) compute
