
This project implements a Drosophila behavior analysis pipeline deployed at the EMBL Heidelberg for a study led by Lautaro Gandara, in Justin Crocker's lab.

Deployment is done on the local HPC cluster. Paths to data repositories and software dependencies are hard-coded in the various Makefiles. As a consequence, the following steps can be done from any directory:

```
git clone https://gitlab.com/larvataggerpipelines/pesticides Pipeline
cd Pipeline
make compute
```

The `make compute` step builds the pipeline and runs it.
Only the unprocessed data files are processed, up to a 5000-file limit.

If more than 5000 files have to be processed, run `make compute` again once all jobs are complete (or `bin/compute.sh` to preserve the previously generated log files).
To check whether your jobs are complete, type `squeue -u $USER`. If all the jobs are done, a single line a text will be printed:

```
[flaurent@login02 Pipeline]$ squeue -u gandara
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
[flaurent@login02 Pipeline]$
```

Otherwise the running and/or pending jobs are listed.

To run again the pipeline for some avi files, go to the `/g/crocker/Lautaro/Drugs_screen/Drosophila_data/Automated analysis` directory and delete the corresponding (sub-)directories in there; and then run `make compute` or `bin/compute.sh` again.

The processing steps include, for each assay or video file:
* tracking with *MWT*,
* postprocessing with *Choreography*,
* merging the spine and outline files,
* repeating the above 3 steps with different tracking parameters until the number of tracked objects is as close as possible to the known number of living larvae (see [mwt-container](https://gitlab.com/larvataggerpipelines/mwt-container)),
* postprocessing with custom procedure *TrackingQualityControl.jl* (see `src/TrackingQualityControl/README.md` for more information)
* and tagging larval behavior into discrete actions with *LarvaTagger.jl* and *MaggotUBA*-based tagger *20230311*.
