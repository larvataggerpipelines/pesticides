#!/usr/bin/env bash

# this script should mimick as much as possible bin/convert_annotations.sh
# and generate corresponding paths

prev_plate=
prev_well=
header=1

while read row; do
  if [ -z $header ]; then
    plate=`echo $row | cut -d, -f2`
    well=`echo $row | cut -d, -f3`
    if ! [ "$plate" = "$prev_plate" -a "$well" = "$prev_well" ]; then
      assay=`ls -1 data/raw/$plate/${well}_* | cut -d/ -f4`
      assay="`dirname $assay`/`basename $assay .avi`"
      count=`echo $row | cut -d, -f13`
      if [ "$count" != "NA" -a "$count" != "0" ]; then
        #echo "$assay,0,$count"
        (cd data/raw; ls $plate/${well}_*)
      fi
    fi
    prev_plate=$plate
    prev_well=$well
  else
    header=
  fi
done < $1
