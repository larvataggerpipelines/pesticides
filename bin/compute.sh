#!/bin/bash
# vim: noai:noet:ts=2:sw=2

if ! [ -f Makefile -a -f bin/compute.sh ]; then
	echo "Please run this script from the pipeline root directory"
	exit 1
fi

if [ -z $TAGGER ]; then
	TAGGER=20230311
fi
if [ -z $TARGET_LARVAE_NR ]; then
	TARGET_LARVAE_NR=target_larva_counts.csv
fi
mkdir -p data/interim/apptainer

[ -z $FROM_MAKEFILE ] && make

rm -f data/interim/pending_assays.txt

if [ -d data/raw/Compliant ]; then
SUBDIR=Compliant
else
SUBDIR=.
fi

echo "Listing the video files. This may take a few minutes"
#if ! [ -z $PIPELINE_TEST ]; then
if false; then
	avi_file="BTI1-2_0.2_P1_S1/B2_2023-03-22-134135-0000.avi"
	avi_file="P6/D6_2024-12-04-162005-0000.avi"
	#avi_file="A0002DF000000031/B1_2022-09-08-180154-0000.avi"
	assay=$(dirname $avi_file)/$(basename $avi_file .avi)
	if ! [ -d data/processed/$assay -o -d data/interim/$assay ]; then
		echo $assay >> data/interim/pending_assays.txt
	fi
else
for avi_file in $(cd data/raw && find $SUBDIR -name '*.avi'); do
	assay=$(dirname $avi_file)/$(basename $avi_file .avi)
	if ! [ -d data/processed/$assay -o -d data/interim/$assay ]; then
		echo $assay >> data/interim/pending_assays.txt
	fi
done
fi

if ! [ -f data/interim/pending_assays.txt ]; then
	if [ -z "$avi_file" ]; then
		echo "error: No assays found"
		echo "       Please check the data/raw link points to the Videos directory:"
		ls -l data
		echo "       and there are avi files in there:"
		(cd data/raw && find $SUBDIR -name '*.avi' -print | head -n10)
		exit 1
	else
		echo "All assays are already processed"
		exit 0
	fi
fi

NASSAYS=$(wc -l data/interim/pending_assays.txt | cut -d\  -f1)

echo "$NASSAYS assays to be processed"

# job time limit should also be adjusted (`#SBATCH --time=`)
if [ -z "$ASSAYS_PER_JOB" ]; then
	ASSAYS_PER_JOB=2
fi
NJOBS=$((NASSAYS / ASSAYS_PER_JOB + 1))

if (( $NJOBS > 1000 )); then
	echo "Maximum number of submitted jobs reached: $NJOBS > 1000;"
  echo "please run bin/compute.sh again once all jobs are complete"
	NJOBS=1000
fi

if [ -z "$CPUS_PER_TASK" ]; then
	CPUS_PER_TASK=8
fi

cat <<EOF > data/interim/submit.sh
#!/bin/bash
#SBATCH --job-name=Pesticides
#SBATCH --array=1-$NJOBS%50
#SBATCH --time=05:00:00
#SBATCH --mem=16G
#SBATCH --output=log/slurm-%A_%a.out
#SBATCH --cpus-per-task=$CPUS_PER_TASK

srun data/interim/job.sh
EOF
cat <<EOF > data/interim/job.sh
#!/bin/bash
#SBATCH --cpus-per-task=$CPUS_PER_TASK

JOB=\$SLURM_ARRAY_TASK_ID
ASSAYS=\$(tail -n +\$(((JOB - 1) * $ASSAYS_PER_JOB + 1)) data/interim/pending_assays.txt | head -n$ASSAYS_PER_JOB)

for ASSAY in \$ASSAYS; do

echo "Processing assay \$ASSAY"

# clear previous uncomplete jobs
# first clean-up for buggy paths that should all be gone by now
rm -rf data/interim\$ASSAY
# second clean-up that basically bypasses the placeholder approach mentioned below
rm -rf data/interim/\$ASSAY

SHORT_ASSAY=\$(echo \$ASSAY | rev | cut -d/ -f1 | rev)
if [ -f "data/$TARGET_LARVAE_NR" ]; then
if [ -z "\$(grep \$SHORT_ASSAY data/$TARGET_LARVAE_NR | grep -v ',0,NA')" ]; then
echo "Cannot find \$SHORT_ASSAY listed in $TARGET_LARVAE_NR"
# add placeholder to prevent next pipeline runs to mark the assay as pending
mkdir -p data/interim/\$ASSAY
continue
fi
fi

# example assay: LF_no_DMSO_1_2024-04-03-111328-0000
DATE_TIME=\$(echo \$SHORT_ASSAY | rev | cut -d_ -f1 | rev)
DATE="\$(echo \$DATE_TIME | cut -d- -f1)\$(echo \$DATE_TIME | cut -d- -f2)\$(echo \$DATE_TIME | cut -d- -f3)"
TIME="\$(echo \$DATE_TIME | cut -d- -f4)"
DATE_TIME="\${DATE}_\${TIME}"

if [ "\${#DATE_TIME}" -ne 15 ]; then
echo "Wrong date_time format: \$DATE_TIME"
continue
fi

# background removal
# (if you comment out the below line, restore the original raw data location in apptainer's --bind expressions)
src/TrackingQualityControl/scripts/remove_background.jl data/raw/\${ASSAY}.avi data/interim/\${ASSAY}.avi

if [ -f "data/$TARGET_LARVAE_NR" ]; then

# comment out the following line if mwt-container if version 0.1
MWT_CONTAINER_ARGS="--date-time \$DATE_TIME --target-larvae-nr $TARGET_LARVAE_NR --debug"

apptainer run --containall --bind "\$(realpath data/interim):/data/raw" --bind "\$(realpath data/interim):/data/interim" --bind "\$(realpath data/processed):/data/processed" --bind "\$(realpath data):/data/external" bin/mwt-container.sif \$ASSAY \$MWT_CONTAINER_ARGS

else
# former procedure with static tracking parameters

apptainer exec --containall --bind "\$(realpath data/interim):/data/raw" --bind "\$(realpath data/interim):/data/interim" bin/mwt-container.sif julia --project=/app/mwt-cli /app/mwt-cli/src/mwt-cli.jl "/data/raw/\${ASSAY}.avi" /data/interim --frame-rate 30 --pixel-thresholds 234 246 --size-thresholds 30 50 1000 1500 --pixel-size 0.073 --date-time \$DATE_TIME

if ! [ -f data/interim/"\$DATE_TIME"/20.spine ]; then
echo "Cannot find any Choreography file"
continue
fi
mkdir -p data/processed/"\${ASSAY}"
cp data/interim/"\$DATE_TIME"/20.* data/processed/"\${ASSAY}"/
fi

echo "Cleaning-up the tracking data"
cd src/TrackingQualityControl
julia-1.11 --project=. -e "using TrackingQualityControl; cleanup(\\"\$ASSAY\\")"
cd ../..

echo "Discrete behavior tagging"
JULIA_NUM_THREADS=$CPUS_PER_TASK apptainer run --containall --env JULIA_DEPOT_PATH=/tmp:/usr/local/share/julia --bind "\$(realpath data/processed/\$ASSAY):/data" --bind "\$(realpath data/interim/apptainer):/app/MaggotUBA/data" bin/larvatagger.sif predict /app/MaggotUBA $TAGGER /data/*_clean.spine --data-isolation
mv data/processed/\$ASSAY/predicted.label data/processed/\$ASSAY/predicted.$TAGGER.label

echo "Fixing file permissions"
chmod -Rf a+rX data/processed/\$ASSAY

done
EOF
chmod a+x data/interim/job.sh

mkdir -p log

sbatch data/interim/submit.sh

echo "Type  squeue --me  to check the status of your jobs. At present, this gives:"
squeue -u $USER
