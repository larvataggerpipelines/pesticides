#!/usr/bin/env bash

prev_plate=
prev_well=
header=1

echo "Video_name,Second,Nr_larvae"

while read row; do
  if [ -z $header ]; then
    plate=`echo $row | cut -d, -f2`
    well=`echo $row | cut -d, -f3`
    if ! [ "$plate" = "$prev_plate" -a "$well" = "$prev_well" ]; then
      assay=`ls -1 data/raw/$plate/${well}_* | cut -d/ -f4 | cut -d. -f1`
      count=`echo $row | cut -d, -f13`
      if [ "$count" != "NA" -a "$count" != "0" ]; then
        echo "$assay,0,$count"
      fi
    fi
    prev_plate=$plate
    prev_well=$well
  else
    header=
  fi
done < $1
