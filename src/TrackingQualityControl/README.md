# Tracking quality control

This processing stage comes as a continuation of the Choreography stage.

It takes the existing 20.spine/20.outline files and generates a new version with possibly fewer and shorter tracks, so that tracking artifacts are removed.

The present directory is a Julia project root.

The *TrackingQualityControl.jl* processing stage includes the following steps for each assay independently:

* remove the first 4 seconds of recording,
* determine the median larval body area (for the entire assay) and discard the moving objects whose mean area is greater than (or equal) twice the median area, or less than half the median area,
* extract the arena border from the corresponding video file and discard the tracks that start at the border,
* reject the end of some tracks whose area collapses, based on a change-point detection algorithm.

Note: for the purpose of track-wide or assay-wide area estimation, the first second of each track (after the first 4 seconds of recording) only is used.
