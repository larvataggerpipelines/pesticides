#!/bin/bash
#=
THIS_SCRIPT=$(realpath "${BASH_SOURCE[0]}")
PROJECT_DIR=$(dirname "$(dirname "${THIS_SCRIPT}")")

if [ -z "$JULIA" ]; then
  JULIA=julia-1.11
else
  echo "Using environment variable JULIA= $JULIA"
fi
$JULIA -v

command -v ffmpeg &>/dev/null || module load FFmpeg/7.0.2-GCCcore-13.3.0
command -v ffmpeg

exec $JULIA --project="${PROJECT_DIR}" --color=yes --startup-file=no -q "$THIS_SCRIPT" "$@"
=#

using TrackingQualityControl

function print_help()
    println("Usage: remove_background.jl <original> <processed>")
    println("")
    println("Arguments:")
    println("   <original>   path to the video file to be processed")
    println("   <processed>  path to the output video file with removed background")
    println("")
end

function print_msg(msg...; level="ERROR")
    msg = pushfirst!(collect(msg), level)
    msg = join(msg, ": ")
    println(msg)
    println("")
end

function main(args=ARGS)
    if length(args) in (2, 3)
        if isfile(args[1])
            if length(args) == 3
                args, framerate = args[1:2], args[3]
                remove_background(args...; framerate=framerate)
            else
                remove_background(args...)
            end
        else
            print_msg("Cannot find file", args[1])
            print_help()
        end
    else
        if !isempty(args)
            print_msg("Wrong number of input arguments")
        end
        print_help()
    end
end

main()

