module Ellipses

using Statistics

export Ellipse, fitellipse

struct Ellipse
	a2
	b2
	x0
	y0
	cosθ
	sinθ
	x0′
	y0′
end

function fitellipse(x, y)
	# center
	xm, ym = mean(x), mean(y)
	x .-= xm
	y .-= ym
	# conic equation
	X = hcat(x.^2, x.*y, y.^2, x, y)
	a = sum(X; dims=1) / (transpose(X) * X)
	x2, xy, y2, x, y = a
	# remove orientation
	@assert min(abs(xy / x2), abs(xy / y2)) > 1e-3
	θ = .5 * atan(xy / (y2 - x2))
	cosθ, sinθ = cos(θ), sin(θ)
	x2′ = x2 * cosθ^2 - xy * cosθ * sinθ + y2 * sinθ^2
	xy′ = 0
	y2′ = x2 * sinθ^2 + xy * cosθ * sinθ + y2 * cosθ^2
	x′ = x * cosθ - y * sinθ
	y′ = x * sinθ + y * cosθ
	xm′ = xm * cosθ - ym * sinθ
	ym′ = xm * sinθ + ym * cosθ
	# main ellipse parameters
	@assert x2′ > 0 && y2′ > 0
	f = 1 + x′^2 / (4x2′) + y′^2 / (4y2′)
	a2, b2 = f / x2′, f / y2′
	x0′ = xm′ - x′ / (2x2′)
	y0′ = ym′ - y′ / (2y2′)
	# rotate back to original orientation
	x0 = x0′ * cosθ + y0′ * sinθ
	y0 = y0′ * cosθ - x0′ * sinθ
	return Ellipse(a2, b2, x0, y0, cosθ, sinθ, x0′, y0′)
end

function fitellipse(mask)
	xy = findall(mask)
	x = [Float64(p[2]) for p in xy]
	y = [Float64(p[1]) for p in xy]
	return fitellipse(x, y)
end

function ellipse_equation_lhs(ellipse, x, y)
	a2, b2 = ellipse.a2, ellipse.b2
	x0, y0 = ellipse.x0, ellipse.y0
	cosθ, sinθ = ellipse.cosθ, ellipse.sinθ
	x′ = x - x0
	y′ = y - y0
	x″ = x′ * cosθ - y′ * sinθ
	y″ = x′ * sinθ + y′ * cosθ
	lhs = x″^2 / a2 + y″^2 / b2
end

end
