module TrackingQualityControl

using LinearAlgebra, Statistics, Meshes, PlanarLarvae, PlanarLarvae.Chore
using VideoIO, ImageIO, FileIO, Colors, ProgressMeter

export fitellipse, overlayborder,
	   raw_outline, clean_outline, save_spine_outline,
	   pipeline_root, rawdatadir, processeddatadir,
	   cleanup, remove_background

include("area.jl")

include("changepoint.jl")

include("Ellipses.jl")
using .Ellipses

include("border.jl")

include("repository.jl")

include("cleanup.jl")

include("background.jl")

end
