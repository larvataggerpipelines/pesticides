## from notebook 01_tracking_quality_control

function movepointat(chain, i; ϵ=1e-3)
	n = length(chain)
	curr = chain[i]
	# when we jump back to the end or beginning, we need to skip one point, because the first and last points are duplicates
	prev = chain[i == 1 ? n - 1 : i - 1]
	next = chain[i == n ? 2 : i + 1]
	dr = (prev - curr) + (next - curr)
	dr /= sqrt(dot(dr, dr))
	return curr + ϵ * dr
end

# indiscriminately move points away; dirty, but efficient
untievertices(chain; kwargs...) = [movepointat(chain, i; kwargs...) for i in 1:length(chain)]

function removeduplicates(chain)
	newchain = empty(chain)
	n = length(chain)
	i = 1
	while i <= n
		point = chain[i]
		push!(newchain, point)
		j = i + 1
		while j <= n && point == chain[j]
			j += 1
		end
		i = j
	end
	if newchain[end] != newchain[1]
		push!(newchain, newchain[1])
	end
	return newchain
end

function removecollinear(chain; atol=1e-7)
	newchain = empty(chain)
	n = length(chain)
	prev = chain[end-1]
	for i in 1:n-1
		curr = chain[i]
		next = chain[i + 1]
		u, v = curr - prev, next - curr
		if dot(u, v)^2 < dot(u, u) * dot(v, v) - atol
			push!(newchain, curr)
		end
		prev = curr
	end
	if newchain[end] != newchain[1]
		push!(newchain, newchain[1])
	end
	return newchain
end

# to be optimised
cleanupoutline = removecollinear ∘ removeduplicates ∘ removecollinear ∘ removeduplicates

function area end

area(outline) = try
    Meshes.area(PolyArea(Chain(untievertices(cleanupoutline(outline)))))
catch
    @error "Failed to compute area within outline"
    0
end

area(state::NamedTuple) = area(outline(Point, state))

area(larva::PlanarLarvae.TimeSeries) = [area(state) for (_, state) in larva]

function medianarea(assay; window=1, calibrationtime=4, framerate=nothing)
	samplesize = nothing
	meanareas = Float64[]
	for larva in values(assay)
		t = times(larva)
		if isnothing(samplesize)
			if isnothing(framerate)
				framerate = 1 / (t[2] - t[1])
			end
			samplesize = round(Int, window * framerate)
		end
		if samplesize <= length(t) && calibrationtime + window <= t[end]
			k = searchsortedfirst(t, calibrationtime)
			area′= area(larva[k:k-1+samplesize])
			push!(meanareas, mean(area′))
		end
	end
	@assert !isempty(meanareas)
	return median(meanareas)
end

function filterbyarea(larva, low, high; window=1, calibrationtime=4, framerate=nothing)
	t = times(larva)
	if isnothing(framerate)
		framerate = 1 / (t[2] - t[1])
	end
	samplesize = round(Int, window * framerate)
	sample = if samplesize <= length(t) && calibrationtime + window <= t[end]
		k = searchsortedfirst(t, calibrationtime)
		larva[k:k-1+samplesize]
	else
		larva[max(1, end-samplesize+1):end]
	end
	return all(low .<= area(sample) .<= high)
end

function filterbyarea(larva, refarea; low=0.5, high=1.5, kwargs...)
	filterbyarea(larva, low * refarea, high * refarea; kwargs...)
end

