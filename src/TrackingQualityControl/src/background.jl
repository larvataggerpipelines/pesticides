
function remove_background(original, processed; framerate=30, white_threshold=.95, kwargs...)
    #println(VideoIO.avcodec_find_encoder_by_name("libx264")) # found
    #println(VideoIO.avcodec_find_encoder_by_name("libx264rbg")) # not found
    #println(VideoIO.avcodec_find_encoder_by_name("ffv1")) # found

    kwargs = Dict{Symbol, Any}(kwargs)
    kwargs[:framerate] = framerate

    frames = VideoIO.get_number_frames(original)

    T = m = n = fullmovie = nothing # set scope
    VideoIO.openvideo(original; target_format=VideoIO.AV_PIX_FMT_GRAY8) do reader
        #kwargs[:framerate] = VideoIO.framerate(reader) # returns 0//1
        #frames = VideoIO.counttotalframes(reader)
        firstframe = read(reader)
        T = eltype(firstframe) # Gray{N0f8}
        m, n = size(firstframe)
        fullmovie = Array{T, 3}(undef, m, n, frames)
        fullmovie[:, :, 1] = firstframe
        @showprogress desc="Reading frames..." for k in 2:frames
            fullmovie[:, :, k] = read(reader)
        end
    end

    background = Matrix{Gray{Float32}}(undef, m, n)
    @showprogress desc="Computing background..." for i in 1:m, j in 1:n
        background[i, j] = median(fullmovie[i, j, :])
    end

    @showprogress desc="Removing background..." for k in 1:size(fullmovie, 3)
        fullmovie[:, :, k] = 1 .- max.(0, background - fullmovie[:, :, k])
    end

    if !isnothing(white_threshold)
        @showprogress desc="Upper thresholding..." for k in 1:size(fullmovie, 3)
            frame = fullmovie[:, :, k]
            @. frame[white_threshold < frame] = 1.
            fullmovie[:, :, k] = frame
        end
    end

    kwargs[:codec_name] = "ffv1"
    #kwargs[:encoder_options] = (crf=0, preset="ultrafast")
    kwargs[:encoder_options] = (level=3, context=1, threads=8)
    T = RGB{eltype(T)} # RGB{N0f8}
    mkpath(dirname(processed))
    open_video_out(processed, T, (m, n); kwargs...) do writer
        @showprogress desc="Writing frames..." for k in 1:size(fullmovie, 3)
            frame = T.(fullmovie[:, :, k])
            write(writer, frame)
        end
    end
end

