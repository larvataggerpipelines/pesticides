
# let us assume the arena is a disk of constant diameter
function radialdistance(points, border::Ellipse; pixelsize=0.073)
	center = Point(pixelsize * border.x0, pixelsize * border.y0)
	radialdistance(points, center)
end

function getends(state)
	spine′= spine(Point, state)
	return [spine′[1], spine′[end]]
end

function radialdistance(larva, center::Point)
	#track = larvatrack(larva)
	#radialdistance.(track, center)
	# pick the head or tail instead
	[(t, maximum(radialdistance.(getends(state), center))) for (t, state) in larva]
end

function radialdistance(point::Point, center::Point)
	Δ = point - center
	sqrt(dot(Δ, Δ))
end

function readfirstframe(avifile)
	VideoIO.openvideo(avifile) do f
		read(f)
	end
end

function getmask(img; threshold=0.6)
	mask = @. Float64(img) < threshold
	h, w = size(mask)
	mask[h÷4:3*h÷4, w÷4:3*w÷4] .= 0
	return mask
end

function Ellipses.fitellipse(moviefile::String)
	img = readfirstframe(moviefile)
	mask = getmask(Gray.(img))
	fitellipse(mask)
end

function mappixels!(f, img)
	h, w = size(img)
	for i in 1:w, j in 1:h
		val = f(Float64(i), Float64(j))
		if !ismissing(val)
			img[j, i] = val
		end
	end
	return img
end

function overlayborder(img, border::Ellipse; color=colorant"red")
	mappixels!(img) do x, y
		eq = Ellipses.ellipse_equation_lhs(border, x, y) - 1
		abs(eq) < .005 ? color : missing
	end
end

function overlayborder(moviefile::String, border::Ellipse; kwargs...)
	img = readfirstframe(moviefile)
	overlayborder(img, border; kwargs...)
end

function overlayborder(moviefile::String; kwargs...)
	img = readfirstframe(moviefile)
	mask = getmask(Gray.(img))
	border = fitellipse(mask)
	overlayborder(img, border; kwargs...)
end

function overlayborder(outputimage::String, moviefile::String, args...; kwargs...)
	save(outputimage, overlayborder(moviefile, args...; kwargs...))
end

