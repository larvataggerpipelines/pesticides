
function calibrate(change_in_mean, series)
	μ = mean(series)
	σ2 = var(series)
	μ′= change_in_mean(μ)
	a = (μ′- μ) / σ2
	b = (μ′+ μ) / 2
	return x -> a * (x - b)
end

struct Opposite
	f
end

opposite(f) = Opposite(f)
opposite(f::Opposite) = f.f

apply(f, x) = f(x)
apply(f::Opposite, x) = -f.f(x)

mutable struct CuSum2{T}
	log_likelihood_ratio
	threshold::T
	S::Vector{Vector{T}}
	G::T
	changepoint::Vector{Int}
end

CuSum2{T}(log_likelihood_ratio, threshold) where {T} = CuSum2{T}(log_likelihood_ratio, threshold, [T[]], 0, Int[])
CuSum2(log_likelihood_ratio, threshold::T) where {T} = CuSum2{T}(log_likelihood_ratio, threshold)

function cusum!(cs::CuSum2{T}, x::T) where {T}
	haschanged(cs) && restart!(cs)
	s = cs.log_likelihood_ratio(x)
	S = cs.S[end]
	push!(S, isempty(S) ? s : S[end] + s)
	cs.G = abs(cs.G + s)
	haschanged(cs) && changepoint!(cs)
end

haschanged(cs::CuSum2) = cs.threshold < cs.G

function restart!(cs::CuSum2{T}) where {T}
	if 0 < cs.G
		push!(cs.S, T[])
		cs.G = 0
		cs.log_likelihood_ratio = opposite(cs.log_likelihood_ratio)
	end
end

function changepoint!(cs)
	changepoint = sum(length, cs.S[1:end-1]; init=0) + argmin(cs.S[end])
	push!(cs.changepoint, changepoint)
end

lastchangepoint(cs::CuSum2) = cs.changepoint[end]
