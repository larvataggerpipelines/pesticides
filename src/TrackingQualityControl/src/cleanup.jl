
function cleanup(plate::String, assay::String; saveborder=true)
	outputfile = clean_outline(plate, assay)
	trackdata = load_outline(raw_outline(plate, assay))
	borderfile = saveborder ? joinpath(dirname(outputfile), "border.png") : nothing
	cleandata = cleanup(trackdata, raw_video(plate, assay); borderfile=borderfile)
	save_spine_outline(outputfile, cleandata)
	return outputfile
end

function cleanup(trackdata, videofile; borderfile=nothing)
	@assert !(trackdata isa String) "Dispatch error"
	refarea = medianarea(trackdata)
	border = fitellipse(videofile)
	if !isnothing(borderfile)
		overlayborder(borderfile, videofile, border)
	end
	cleandata = empty(trackdata)
	for (trackid, track) in pairs(trackdata)
		track = discardfirstseconds(track)
		isnothing(track) && continue
		filterbyarea(track, refarea) || continue
		track = filterbydistance(track, border)
		isnothing(track) && continue
		track = changepointdetection(track)
		cleandata[trackid] = track
	end
	return cleandata
end

function discardfirstseconds(track; seconds=4, minduration=1)
	k = searchsortedfirst(times(track), seconds)
	track = track[k:end]
	if !isempty(track)
		tstart, _ = track[1]
		tend, _ = track[end]
		if minduration <= tend - tstart
			return track
		end
	end
end

function filterbydistance(track, ellipse; maxdist=48, minduration=1)
	dist = radialdistance(track, ellipse)
	k = findfirst(d -> maxdist <= d[2], dist)
	isnothing(k) && return track
	k == 1 && return nothing
	track = track[1:k-1]
	t0, _ = track[1]
	t1, _ = track[end]
	return minduration <= t1 - t0 ? track : nothing
end

function changepointdetection(track)
	areas = area(track)
	t0, _ = track[1]
	firstsecond = searchsortedfirst(times(track), t0 + 1)
	llratio = calibrate(areas[1:firstsecond]) do μ
		0.5μ
	end
	cs = CuSum2(llratio, 1e3)
	for x in areas
		cusum!(cs, x)
		if haschanged(cs)
			stopat = lastchangepoint(cs)
			return track[1:stopat]
		end
	end
	return track
end

function cleanup(plate_assay; kwargs...)
	# originally, the repository was structured with plates as top directory names,
	# with assays as direct child directories, but later began to include arbritrary
	# directory names and structures, with assays 2 levels below the top directories;
	# as a consequence, `plate` can be a relative path instead of a directory name.
	assay, plate = (string ∘ reverse).(split(reverse(plate_assay), '/'; limit=2))
	cleanup(plate, assay; kwargs...)
end

function cleanup(; kwargs...)
	for plate in readdir(processeddatadir())
		platedir = joinpath(processeddatadir(), plate)
		isdir(platedir) || continue
		for assay in readdir(platedir)
			assaydir = joinpath(platedir, assay)
			isdir(assaydir) || continue
			isempty(readdir(assaydir)) && continue
			cleanup(plate, assay; kwargs...)
		end
	end
end

