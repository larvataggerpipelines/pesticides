
project_root() = dirname(Base.current_project())
pipeline_root() = realpath(joinpath(project_root(), "..", ".."))
#@assert isfile(joinpath(pipeline_root(), "Makefile"))
#@assert isdir(joinpath(pipeline_root(), "data"))

rawdatadir() = joinpath(pipeline_root(), "data", "raw")
processeddatadir() = joinpath(pipeline_root(), "data", "processed")

function raw_outline(plate, assay)
	joinpath(processeddatadir(), plate, assay, "20.outline")
end

function clean_outline(plate, assay; suffix="clean")
	filename = assay
	isempty(suffix) || (filename = join((filename, suffix), '_'))
	filename = filename * ".outline"
	joinpath(processeddatadir(), plate, assay, filename)
end

function raw_video(plate, assay)
	joinpath(rawdatadir(), plate, assay * ".avi")
end

function load_outline(outlinefile)
    trackdata = read_chore_files((:spine=>Spine, :outline=>Outline), outlinefile)
    return first(values(trackdata))
end

function save_spine_outline(outlinefile, trackdata)
	assay = basename(dirname(outlinefile))
	datetime = split(assay, '_')[end]
	year, month, day, time, _ = split(datetime, '-')
	date_time = "$(year)$(month)$(day)_$(time)"
	if length(date_time) != 15
		error("Cannot determine assay date and time from directory name: \"$assay\"")
	end
	val = x -> round(x; digits=3)
	@assert endswith(outlinefile, ".outline")
	spinefile = outlinefile[1:end-7] * "spine"
	open(spinefile, "w") do spinefile
		open(outlinefile, "w") do outlinefile
			for (trackid, timeseries) in pairs(trackdata)
				trackid = lpad(trackid, 5, '0')
				for (t, state) in timeseries
					t = val(t)
					for (file, feature) in ((spinefile, spine), (outlinefile, outline))
						write(file, "$date_time $trackid $t")
						for point in feature(Point, state)
							x, y = coordinates(point)
							write(file, " $(val(x)) $(val(y))")
						end
						write(file, "\n")
					end
				end
			end
		end
	end
end

