using TrackingQualityControl
using Test
using PlanarLarvae, PlanarLarvae.Chore
using FileIO

samplevideo = joinpath(rawdatadir(), "A0001DF000000031", "A1_2022-07-12-150920-0000.avi")

processeddatadir = joinpath(pipeline_root(), "data", "processed")
sampletracking = joinpath(processeddatadir(), "A0001DF000000031", "A1_2022-07-12-150920-0000", "20.outline")

@testset "TrackingQualityControl.jl" begin
    border = fitellipse(samplevideo)
    testborderfile = joinpath(dirname(sampletracking), "test_border.png")
    overlayborder(testborderfile, samplevideo, border)
    expectedborderfile = joinpath(dirname(sampletracking), "border.png")
    @test read(testborderfile) == read(expectedborderfile)
    rm(testborderfile)

    trackdata = read_chore_files((:spine=>Spine, :outline=>Outline), sampletracking)
    trackdata = first(values(trackdata))
    cleandata = cleanup(trackdata, samplevideo)
    test_outline = clean_outline("A0001DF000000031", "A1_2022-07-12-150920-0000"; suffix="test")
    save_spine_outline(test_outline, cleandata)
    expected_outline = test_outline[1:end-12] * "clean.outline"
    @test readlines(test_outline) == readlines(expected_outline)
    test_spine = test_outline[1:end-7] * "spine"
    expected_spine = expected_outline[1:end-7] * "spine"
    @test readlines(test_spine) == readlines(expected_spine)
    rm(test_outline)
    rm(test_spine)

end
